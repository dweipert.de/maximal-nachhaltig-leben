# Summary

[Einleitung](./introduction.md)

---

- [Lebensmittel](./food/Readme.md)
  - [Essen](./food/eat.md)
  - [Trinken](./food/drink.md)

- [Kleidung]()
  - [Oben]()
  - [Unten]()
  - [Waschen]()

- [Hygiene]()

- [Von A nach B kommen]()

- [Sport]()

- [Technologie]()

